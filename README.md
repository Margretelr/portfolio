### [Link to portfolio: https://margretelr.gitlab.io/portfolio/](https://margretelr.gitlab.io/portfolio/)

#### To do list:
- add all works as underpages (status: all digital design done)
- Figure out how best to display Graphic design
- Fix image sizes in Graphic design and Programming so they're all the same
- compressed images and tidy up unused files
- load images at the same time?
- fix unresponsive image size (on the left column) of workGallery (this fixed itself I think?)
- website metadata
- burger menu and general responsiveness on smaller devices
- disable flower cursor on mobile devices - add something else?
- add CV?

